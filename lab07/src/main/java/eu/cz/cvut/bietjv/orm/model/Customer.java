package eu.cz.cvut.bietjv.orm.model;
import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.util.List;

@Entity
public class Customer {

    @Id
    @Column(name="Cust_ID")
    private int perCode;
    private String name;
    private String surname;
    @OneToMany(mappedBy = "")
    private List<Product> products;

    public Customer(String name, String surname) {
        this.name = name;
        this.surname=surname;
    }

    public Customer(){

    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Customer: "+this.name +" "+this.surname;
    }

}
